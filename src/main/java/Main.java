import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class Main {
    private static String FILE = "C:/tp/pdf.pdf";
    private static Font font = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);

    public static void main(String[] args) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addContent(document);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void addTitlePage(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        document.add(preface);
        document.newPage();
    }
    private static void addContent(Document document) throws DocumentException {
        Paragraph title = new Paragraph("Resume", font);
        title.setAlignment(Element.ALIGN_CENTER);
        document.add(title);
        createTable(document);
    }

    private static void createTable(Document document)
            throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.setWidths(new int[]{3, 3});
        table.addCell("First name");
        table.addCell("Marcin");
        table.addCell("Last name");
        table.addCell("Ryznar");
        table.addCell("Education");
        table.addCell("2018 - 2022 PWSZ Tarnów");
        table.addCell("Summary");
        table.addCell("                \n\n\n\n");
        document.add(table);
    }


}